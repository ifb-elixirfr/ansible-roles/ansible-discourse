This role is a customized version of

https://github.com/axilleas/ansible-role-discourse

ansible-role-discourse
======================

This role installs Docker and Discourse with self signed certificates for the dev and preprod environments and Renater certificates for the prod environment
(https://discourse.org).

REQUIREMENT:
Make sure there is at least 10 GB available in /var on the target machine before running this playbook (default template has 5 GB)

# To add a plugin:

Add the url for the plugin depot in the variable file: defaults/main.yml
For example
discourse_plugins:
   - https://github.com/discourse/discourse-solved.git


# To configure a redirection:

Edit the template templates/app.yml.j2
```
  after_web_config:
    - file:
        path: /etc/nginx/conf.d/discourse_redirect_1.conf
        contents: |
          server {
            listen 80 default_server;
            listen [::]:80 default_server;
            server_name _;
            return 301 https://$host$request_uri;
          }
    - file:
        path: /etc/nginx/conf.d/discourse_redirect_2.conf
        contents: |
          server {
            listen 80;
            server_name {{ discourse_old_url }};
            return 301 $scheme://{{ discourse_new_url }}$request_uri;
          }
```

# To generate new self signed certificates, remove manually the old certificates

```
rm  /var/discourse/shared/standalone/ssl/ssl.crt
rm  /var/discourse/shared/standalone/ssl/ssl.key
 
``` 
